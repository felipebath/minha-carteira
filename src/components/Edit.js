import React, { Component } from 'react';
import firebase from '../Firebase';
import { Link } from 'react-router-dom';

class Edit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      key: '',
      nome: '',
      sigla: ''
    };
  }

  componentDidMount() {
    const ref = firebase.firestore().collection('boards').doc(this.props.match.params.id);
    ref.get().then((doc) => {
      if (doc.exists) {
        const board = doc.data();
        this.setState({
          key: doc.id,
          nome: board.nome,
          sigla: board.sigla
        });
      } else {
        console.log("No such document!");
      }
    });
  }

  onChange = (e) => {
    const state = this.state
    state[e.target.name] = e.target.value;
    this.setState({board:state});
  }

  onSubmit = (e) => {
    e.preventDefault();

    const { nome, sigla } = this.state;

    const updateRef = firebase.firestore().collection('boards').doc(this.state.key);
    updateRef.set({
      nome,
      sigla
    }).then((docRef) => {
      this.setState({
        key: '',
        nome: '',
        sigla: ''
      });
      //this.props.history.push("/show/"+this.props.match.params.id)
      this.props.history.push("/")
    })
    .catch((error) => {
      console.error("Error adding document: ", error);
    });
  }

  render() {
    return (
      <div class="container">
        <div class="panel panel-default">
          <div class="panel-heading">
            <br/>
            <h3 class="panel-title">
              <Link to={`/`}>MINHA CARTEIRA</Link> / EDITAR
            </h3>
          </div>
          <div class="panel-body">
            <br/> 
            <h4><Link to={`/show/${this.state.key}`} class="btn btn-primary">Voltar</Link></h4>
            <br/>
            <form onSubmit={this.onSubmit}>
              <div class="form-group">
                <label for="title">Sigla:</label>
                <input type="text" class="form-control" name="sigla" value={this.state.sigla} onChange={this.onChange} placeholder="Sigla" />
              </div>
              <div class="form-group">
                <label for="description">Nome:</label>
                <input type="text" class="form-control" name="nome" value={this.state.nome} onChange={this.onChange} placeholder="Nome" />
              </div>
              <button type="submit" class="btn btn-success">Salvar</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Edit;
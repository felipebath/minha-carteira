import React, { Component } from 'react';
import firebase from '../Firebase';
import { Link } from 'react-router-dom';

class Show extends Component {

  constructor(props) {
    super(props);
    this.state = {
      board: {},
      key: ''
    };
  }

  componentDidMount() {
    const ref = firebase.firestore().collection('boards').doc(this.props.match.params.id);
    ref.get().then((doc) => {
      if (doc.exists) {
        this.setState({
          board: doc.data(),
          key: doc.id,
          isLoading: false
        });
      } else {
        console.log("No such document!");
      }
    });
  }

  delete(id){
    firebase.firestore().collection('boards').doc(id).delete().then(() => {
      console.log("Document successfully deleted!");
      this.props.history.push("/")
    }).catch((error) => {
      console.error("Error removing document: ", error);
    });
  }

  render() {
    return (
      <div class="container">
        <div class="panel panel-default">
          <div class="panel-heading">
            <br/>
            <h3 class="panel-title">
              MINHA CARTEIRA
            </h3>
          </div>
          <div class="panel-body">
            <br/>    
            <h4><Link to="/" class="btn btn-primary">Listar Todos</Link></h4>
            <br/>
            <table class="table table-stripe">
              <thead>
                <tr>
                  <th>Sigla</th>
                  <th>Nome</th>
                </tr>
              </thead>
              <tbody>
                  <tr>
                    <td>{this.state.board.sigla}</td>
                    <td>{this.state.board.nome}</td>
                  </tr>
              </tbody>
            </table>
            <br/>
          </div>
          <div class="panel-body">
            <Link to={`/edit/${this.state.key}`} class="btn btn-success">Editar</Link>&nbsp;
            <button onClick={this.delete.bind(this, this.state.key)} class="btn btn-danger">Apagar</button>
          </div>
        </div>
      </div>
    );
  }
}

export default Show;
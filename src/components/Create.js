import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import firebase from '../Firebase';
import { Link } from 'react-router-dom';

class Create extends Component {

  constructor() {
    super();
    this.ref = firebase.firestore().collection('boards');
    this.state = {
      nome: '',
      sigla: ''
    };
  }
  onChange = (e) => {
    const state = this.state
    state[e.target.name] = e.target.value;
    this.setState(state);
  }

  onSubmit = (e) => {
    e.preventDefault();

    const { nome, sigla } = this.state;

    this.ref.add({
      nome,
      sigla
    }).then((docRef) => {
      this.setState({
        nome: '',
        sigla: ''
      });
      this.props.history.push("/")
    })
    .catch((error) => {
      console.error("Error adding document: ", error);
    });
  }

  render() {
    const { nome, sigla } = this.state;
    return (
      <div class="container">
        <div class="panel panel-default">
          <div class="panel-heading">
            <br/>
            <h3 class="panel-title">
              MINHA CARTEIRA / ADICIONAR AÇÃO
            </h3>
            <br/>
          </div>
          <div class="panel-body">
            <h4><Link to="/" class="btn btn-primary">Listar Todos</Link></h4>
            <br/>
            <form onSubmit={this.onSubmit}>
              <div class="form-group">
                <label for="sigla">Sigla:</label>
                <input type="text" class="form-control" name="sigla" value={sigla} onChange={this.onChange} placeholder="Sigla" />
              </div>
              <div class="form-group">
                <label for="nome">Nome:</label>
                <input type="text" class="form-control" name="nome" value={nome} onChange={this.onChange} placeholder="Nome" />
              </div>
              <button type="submit" class="btn btn-success">SALVAR</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Create;
import * as firebase from 'firebase';
import firestore from 'firebase/firestore'

const settings = {timestampsInSnapshots: true};

const config = {
  apiKey: "AIzaSyDrqcu6P_23iX1Z5xraDdygtDKcyZ-DHyg",
  authDomain: "react-firestore-bbb42.firebaseapp.com",
  databaseURL: "https://react-firestore-bbb42.firebaseio.com",
  projectId: "react-firestore-bbb42",
  storageBucket: "react-firestore-bbb42.appspot.com",
  messagingSenderId: "912231619180",
  appId: "1:912231619180:web:37f71aef9bc3f4db"
};

firebase.initializeApp(config);

firebase.firestore().settings(settings);

export default firebase;
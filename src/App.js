import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './App.css';
import firebase from './Firebase';

class App extends Component {
  constructor(props) {
    super(props);
    this.ref = firebase.firestore().collection('boards');
    this.unsubscribe = null;
    this.state = {
      boards: []
    };
  }

  onCollectionUpdate = (querySnapshot) => {
    const boards = [];
    querySnapshot.forEach((doc) => {
      const { sigla, nome } = doc.data();
      boards.push({
        key: doc.id,
        doc, // DocumentSnapshot
        sigla,
        nome,
      });
    });
    this.setState({
      boards
   });
  }

  componentDidMount() {
    this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
  }

  render() {
    return (
      <div class="container">
        <div class="panel panel-default">
          <div class="panel-heading">
            <br/>
            <h3 class="panel-title">
              MINHA CARTEIRA
            </h3>
          </div>
          <div class="panel-body">
            <br/>  
            <h4><Link to="/create" class="btn btn-primary">Adicionar Ação</Link></h4>
            <br/>  
            <table class="table table-stripe">
              <thead>
                <tr>
                  <th>Sigla</th>
                  <th>Nome</th>
                  <th>Cotação</th>
                </tr>
              </thead>
              <tbody>
                {this.state.boards.map(board =>
                  <tr>
                    <td><Link to={`/show/${board.key}`}>{board.sigla}</Link></td>
                    <td><Link to={`/show/${board.key}`}>{board.nome}</Link></td>
                    <td>R$ 14,63</td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default App;